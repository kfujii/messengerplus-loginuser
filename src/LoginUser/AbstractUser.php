<?php

namespace Sharecoto\LoginUser;

abstract class AbstractUser
{
    protected $user;

    /**
     * @var string
     */
    protected $type;

    public function type()
    {
        assert(is_string($this->type));
        return $this->type;
    }

    /**
     * get $user property
     *
     * @param string $name 
     */
    abstract public function get($name);

    /**
     * return user profile pic
     *
     * @return string
     */
    abstract public function picture();

    /**
     * return user permission
     *
     * @return bool
     */
    abstract public function hasPermission($perm);
}
